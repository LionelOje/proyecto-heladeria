var btnMenu = document.getElementById("btn-menu");
var menu = document.getElementById("menu");//traigo a dos variables los id del boton del menu y lo que seria el contenido donde esta contactos,galeria etc para trabajarlos en js

btnMenu.addEventListener("click",function(){ // a la variable que contiene el boton de menu le digo que cuando se haga click en el me ejecute una funcion 
    menu.classList.toggle("mostrar"); // la funcion lo que hace es con el class.. crear una clase llamada mostrar. para que yo en css le ponga el estilo hight que seria que aparesca el contenido del menu que habia echo desaparecer en la clase .menu en css con el higt 0. Osea con esto lo que hago es que la propiedad de esta nueva clase (mostrar) se ejecute en la variable menu que tiene el contenido del mismo. Entonses cuando hago click en el boton de menu me lo hace aparecer o desaparecer VER EN CSS la propiedad transition y la clase .mostrar
})




//Todos los efectos de scroll tienen el mismo funcionamiento. Primero se captura la posicion del eje y de la pantalla con document.documentElement.scrollTop, luego se captura la posicion del elemento que quiero que tenga el efecto con el offsetTop, y luego se compara la posicion Y del elemento - un valor, con la posicon del eje y de la pantalla. Si la posicion del elemento - el valor, es menor a la posicion de la pantalla, quiere decir que el scroll ya paso por esa cordenada, lo que hace que se me active el efecto en el elemento.


//////////////////EFECTOS INDEX/////////////////////////////////////


/////Este efecto de scroll se aplica al primer cartel que aparece que dice, Heladerias pituco la mejor calidad

var primerEfecto = document.querySelector("#primerEfecto");

function primerScroll(){

    // Funcionamiento del scroll explicado en la linea 11

    var posicionScrollMouse = document.documentElement.scrollTop;
    var PosicionElemento1 = primerEfecto.offsetTop;
    if (PosicionElemento1 -500 < posicionScrollMouse){
        primerEfecto.style.opacity = 1;
    }
}

window.addEventListener("scroll",primerScroll);



///// Este segundo scroll se aplica a las primera imagen de un cono de helado que aparece y a lo que esta al lado que dice ventas minoristas etc.

//Los traigo por nombre de clases y por id, asi les pongo los dos efectos a la ves, el de cambiar una propiedad css y el de agregar una clase con un keyframe 

var segundoEfecto = document.querySelectorAll(".scroll2");
var segundoEfectoA = document.querySelector("#segundoEfectoA");
var segundoEfectoB = document.querySelector("#segundoEfectoB");

function segundoScroll(){                      
    
    // Funcionamiento del scroll explicado en la linea 11, con la diferencia de que aca le estoy aplicando el efecto a dos elementos a la ves, por eso los puse dentro de un for 

    var posicionScrollMouse = document.documentElement.scrollTop;
    for (i=0; i<segundoEfecto.length; i++){
        var posicionElemento2 = segundoEfecto[i].offsetTop;
        if(posicionElemento2 -500 < posicionScrollMouse){
            segundoEfecto[i].style.opacity = 1;
            segundoEfectoA.classList.add("AnimacionSegundoEfectoA");
            segundoEfectoB.classList.add("AnimacionSegundoEfectoB");
        }
    }
n.slideDown(300);
}

window.addEventListener("scroll",segundoScroll);



/////Este efecto scroll se aplica al titulo especiales del mes

var tercerEfecto = document.querySelector("#tercerEfecto");

function tercerScroll(){
    
    // Funcionamiento del scroll explicado en la linea 11. Este me cabia la opasidad asi aparece despacito 
    
    var posicionScrollMouse = document.documentElement.scrollTop;
    var PosicionElemento3 = tercerEfecto.offsetTop;
    if (PosicionElemento3 -500 < posicionScrollMouse){
        tercerEfecto.style.opacity = 1;
    }
}

window.addEventListener("scroll",tercerScroll);



////Este scroll se aplican a las 4 imagenes de helados especiales del mes. Su funcionamiento es igual al del segundo scroll explicado a partir de la linea 34, con la diferencia de que a las dos primeras imagenes, se le aplica un efecto y a los dos ultimas se les aplica otro distinto, por medio de keyfram

var cuartoEfecto = document.querySelectorAll(".scroll3");
var cuartoEfectoA = document.querySelector("#cuartoEfectoA");
var cuartoEfectoB = document.querySelector("#cuartoEfectoB");
var cuartoEfectoC = document.querySelector("#cuartoEfectoC");
var cuartoEfectoD = document.querySelector("#cuartoEfectoD");

function cuartoScroll(){                      
    
    var posicionScrollMouse = document.documentElement.scrollTop;
    for (i=0; i<cuartoEfecto.length; i++){
        var posicionElemento4 = cuartoEfecto[i].offsetTop;
        if(posicionElemento4 -500 < posicionScrollMouse && i<2){
            cuartoEfecto[i].style.opacity = 1;
            cuartoEfectoA.classList.add("AnimacionCuartoEfectoB");
            cuartoEfectoB.classList.add("AnimacionSegundoEfectoA");
        }
        else if(posicionElemento4 -500 < posicionScrollMouse && i>=2){
            cuartoEfecto[i].style.opacity = 1;
            cuartoEfectoC.classList.add("AnimacionCuartoEfectoB");
            cuartoEfectoD.classList.add("AnimacionSegundoEfectoA");
        }
    }

}

window.addEventListener("scroll",cuartoScroll);




//////////////////////////////EFECTO PAGINA CONTACTO//////////////////////


/////Este efecto de scroll se aplica a la seccion de, informacion de contacto y mandar un email.

var efectoElemento = document.querySelector(".scrollContacto");

function UnicoScrollContacto (){
    
    // Funcionamiento del scroll explicado en la linea 11. Este me cabia la opasidad asi aparece despacito

    var posicionScrollMouse = document.documentElement.scrollTop;
        var posicionElementoContacto = efectoElemento.offsetTop;
        if(posicionElementoContacto -500 <posicionScrollMouse){
            efectoElemento.style.opacity = 1;
        }
    }

window.addEventListener("scroll",UnicoScrollContacto);




/////////////////////EFECTO PAGINA GALERIA ////////////////


/////Este efecto de scroll se aplica a la seccion de las imagenes 

var efectoGaleria = document.querySelector(".scrollGaleria");

function UnicoScrollGaleria (){
    
    // Funcionamiento del scroll explicado en la linea 11. Este me cabia la opasidad asi aparece despacito

    var posicionScrollMouse = document.documentElement.scrollTop;
        var posicionElementoGaleria= efectoGaleria.offsetTop;
        if(posicionElementoGaleria -500 < posicionScrollMouse){
            efectoGaleria.style.opacity = 1;
        }
    }

window.addEventListener("scroll",UnicoScrollGaleria);




////////////////EFECTOS PAGINA NOSOTROS////////////////


/////Este efecto de scroll se aplica al cartel de nuestros origenes 

var origenes = document.querySelector("#origenes");

function scrollNosotros1 (){

    // Funcionamiento del scroll explicado en la linea 11. Este me agrega una clase con un keyfram que me hace aparecer el elemento hacia arriba.
    
    var posicionScrollMouse = document.documentElement.scrollTop;
        var posicionElementoGaleria= origenes.offsetTop;
        if(posicionElementoGaleria -600 < posicionScrollMouse){
            origenes.classList.add("AnimacionOrigenes");
        }
    }

window.addEventListener("scroll",scrollNosotros1);



/////Este efecto de scroll se aplica al titulo nuestro equipo

var efectoNosotros2 = document.querySelector(".scrollNosotros2");

function scrollNosotros2 (){
    
    // Funcionamiento del scroll explicado en la linea 11. Este me cabia la opasidad asi aparece despacito

    var posicionScrollMouse = document.documentElement.scrollTop;
        var posicionElementoGaleria= efectoNosotros2.offsetTop;
        if(posicionElementoGaleria -500 < posicionScrollMouse){
            efectoNosotros2.style.opacity = 1;
        }
    }

window.addEventListener("scroll",scrollNosotros2);



/////Este efecto de scroll se aplica al texto e imagenes de los 3 empleados

//Los traigo por nombre de clases y por id, asi les pongo los dos efectos a la ves, el de cambiar una propiedad css y el de agregar una clase con un keyframe 

var marcelo = document.querySelector("#marcelo");
var damian = document.querySelector("#damian");
var fede = document.querySelector("#fede");

function scrollNosotros3 (){
    
    // Funcionamiento del scroll explicado en la linea 11, con la diferencia de que aca le estoy aplicando el efecto a 3 elementos a la ves, por eso los puse dentro de un for 


    var posicionScrollMouse = document.documentElement.scrollTop;
        var posicionElementoGaleria= marcelo.offsetTop;
        if(posicionElementoGaleria -500 < posicionScrollMouse){
            marcelo.style.opacity = 1;
            damian.style.opacity = 1;
            fede.style.opacity = 1;
            
            marcelo.classList.add("AnimacionMarcelo");
            damian.classList.add("AnimacionDamian");
            fede.classList.add("AnimacionFede");
        }
    }

window.addEventListener("scroll",scrollNosotros3);