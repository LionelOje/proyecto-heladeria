var n = 8;
var Aimagenes = ["img/alta1.jpg","img/petisa1.jpg","img/petisa2.jpg","img/flat-lay-photography-of-desserts-2062883%20(2).jpg","img/alta3nn.jpg","img/petisa3.jpg","img/petisa4.jpg","img/pink-and-white-ice-cream-3186010%20(1).jpg"];
var avanzar = document.querySelector("#avanzarjs");
var retroceder = document.querySelector("#retrocederjs");
var imagenes = document.querySelectorAll(".margenimagenes"); //con esto lo que hago es llamar a todos los elementos que tengan la clase "margenimagenes" osea todas las imagenes de la galeria y las guardo en la variable imagenes 
var modal = document.querySelector("#modaljs"); // con esto lo que hago es llamar al id del contendor del modal que contiene la imagen y el boton y lo guardo en la variable modal
var img = document.querySelector("#modaljs_img");// con esto lo que hago es llamar al id que contiene la etiqueta img dentro del modal, y lo guardo en la variable img
var btn = document.querySelector("#modaljs_btn"); // con esto lo que hago es llamar al contenedor del boton cerar y lo guardo en la variable btn

for (var i=0; i<Aimagenes.length; i++){ // aca hago un for que haga una pasada por todas las imagenes de la galeria y de esa forma poder aplicarle el evento de click con la funcion con lo que quiero que pase cuando se le hace click. como son muchas lo hago asi sino tendria que escribir lo que va aca dentro con todas las imagenes, el .lenght es como que me las cuenta si yo no se cuantas son con esto remplazo el tener que contarlas y poner el numero total en el i<  dentro de los parametros del for
    imagenes[i].addEventListener("click", function(e){// aca le agrego a todas las imagenes por eso uso imagenes[i] el evento on click con la funcion que haga que cuando hago click en alguna imagen de la galeria me abra el modal y la imagen 
        modal.classList.toggle("modal_open"); // cada ves que se haga click al contenedor modal que me contenia el fondo del modal se le asigna la clase modal_open que en css me activaba el modal para que se vea 
        img.setAttribute("src", e.target.src); // si lo dejo asi sin esta parte me abre solo el fondo del modal sin imagen por que no tiene ninguna ruta en el src de la etiqueta img dentro del modal por eso con esto lo que hago es colocar la ruta de la imagen a la que le estoy haciendo click en el src o ruta de la etiqueta imagen que esta dentro del modal y de esta manera se me abre el fondo con la imagen. a la variable img que es la etiqueta img del modal le envio la ruta o atributo la cual consigo con el e.target.src la e hace referencia a la imagen con la que yo estoy trabajando osea la que le ise clik  
        
    });  
    n--;
}

//////Logica de los botones avanzar y retroceder de l galeria. Utiliza un indice que me va pasndo el atributo src al div que contiene el lugar de la imagen, desde un array//////

avanzar.addEventListener("click",function(){
    n++;

    if(n<=7){
    
    img.setAttribute("src", Aimagenes[n]);
}
else{
    n=0
    img.setAttribute("src", Aimagenes[n]);
}   
});


retroceder.addEventListener("click",function(){
    n--;
    if (n>=0){
    img.setAttribute("src", Aimagenes[n]);
}
else{
    n=7
    img.setAttribute("src", Aimagenes[n]);
}
});


btn.addEventListener("click",function(){ // aca hago lo del boton cerrrar le agrego un evento de click que cuando se le haga click me desactive saque la clase de css modal_open. pongo lo mismo por que con classList.toggle si ya hay una clase con el nombre puesto activada, si lo repito me la desactiva. y lo pongo afuera del for por que el boton es uno para todas las imagenes no hay necesidad de repetirlo 10 veces 
    modal.classList.toggle("modal_open");
});



